# -*- coding: utf-8 -*-

from flask import make_response, request, url_for, jsonify
from models import Task
from app import app, db
from flask.ext.httpauth import HTTPBasicAuth

auth = HTTPBasicAuth()

@auth.get_password
def get_password(username):
	"""Returnera lösenordet för en given användare."""
	if username == 'admin':
		return 'secret'
	return None

@auth.error_handler
def unauthorized():
	"""Returnera felmeddelande för misslyckad autentisering."""
	return make_response(jsonify({'error': 'Unauthorized access'}), 401)

@app.errorhandler(404)
def not_found(error):
	"""Den vanliga felhanteraren (för 404) skapar HTML och för att vara RESTful bör vi ha JSON."""
	return make_response(jsonify({'error': 'Not found'}), 404)

def make_public_task(task):
	"""Modifiera en task innan den exporteras. Byt ut id mot en URI till resursen."""
	new_task = {}
	new_task['uri'] = url_for('get_task', id = task.id, _external = True)
	new_task['title'] = task.title
	new_task['description'] = task.description
	new_task['done'] = task.done
	
	return new_task

@app.route('/todo/api/1.0/tasks', methods = ['GET'])
def get_tasks():
	"""Hämta en fullständig lista över alla de tasks som finns."""
	tasks = Task.query.all()
	return jsonify({'tasks': map(make_public_task, tasks)})

@app.route('/todo/api/1.0/tasks/<int:id>', methods = ['GET'])
def get_task(id):
	"""Hämta en task."""
	try:
		task = Task.query.filter(Task.id == id).one()
		return jsonify({'task': make_public_task(task)})
	except:
		return make_response(jsonify({'error': 'Not found'}), 404)

@app.route('/todo/api/1.0/tasks', methods = ['POST'])
@auth.login_required
def create_task():
	"""Skapa en ny task. Endast attributet title är obligatoriskt och attributet done är alltid falskt."""
	if not request.json or not 'title' in request.json:
		return make_response(jsonify({'error': 'Bad request'}), 400)

	task = Task(title = request.json['title'],
				description = request.json.get('description', ''),
				done = False)
	db.session.add(task)
	db.session.commit()
	return jsonify({'task': make_public_task(task)})

@app.route('/todo/api/1.0/tasks/<int:id>', methods = ['PUT'])
@auth.login_required
def update_task(id):
	"""Uppdatera en task. Alla attribut är frivilliga."""
	try:
		task = Task.query.filter(Task.id == id).one()
	except:
		return make_response(jsonify({'error': 'Not found'}), 404)
	
	if not request.json:
		return make_response(jsonify({'error': 'Bad request'}), 400)

	if 'title' in request.json and type(request.json['title']) != unicode:
		return make_response(jsonify({'error': 'Bad request'}), 400)

	if 'description' in request.json and type(request.json['description']) != unicode:
		return make_response(jsonify({'error': 'Bad request'}), 400)

	if 'done' in request.json and type(request.json['done']) != bool:
		return make_response(jsonify({'error': 'Bad request'}), 400)
	
	task.title = request.json.get('title', task.title)
	task.description = request.json.get('description', task.description)
	task.done = request.json.get('done', task.done)
	db.session.commit()
	
	return jsonify({'task': make_public_task(task)})

@app.route('/todo/api/1.0/tasks/<int:id>', methods = ['DELETE'])
@auth.login_required
def delete_task(id):
	"""Radera en task."""
	try:
		task = Task.query.filter(Task.id == id).one()
	except:
		return make_response(jsonify({'error': 'Not found'}), 404)
	
	db.session.delete(task)
	db.session.commit()
	
	return jsonify({'result': True})