#!venv/bin/python

from config import SQLALCHEMY_DATABASE_URI
from app import db, models

db.create_all()

db.session.add(models.Task(title = 'Get introduced to Python',
						   description = 'Read A-byte-of-python to get a good introduction to Python',
						   done = False))
db.session.add(models.Task(title = 'Python website',
						   description = 'Visit the Python website.',
						   done = True))
db.session.add(models.Task(title = 'Choose editor',
						   description = 'Test various editors for and check the syntax highlighting.',
						   done = False))

db.session.commit()